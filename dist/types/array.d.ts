/**
 * 数组去重
 * @param arr 需要去重的数组
 * @returns {Array} 去重后的数组
 */
export declare function removeArrRepeat(arr: any[]): any[];
/**
 * 分割数组subGroupLength份
 * @param array
 * @param subGroupLength
 */
export declare function groupArray(array: any[], subGroupLength: number): any[];
