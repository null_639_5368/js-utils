import { Options } from 'object-to-formdata';
/**
* 数组插入到Formdata
* @example 1.在上传多张图片的时候会用到
* @description 该方法会改变原formdata
* @param {FormData} formData 源formdata
* @param {string} key 数组key值
* @param {Array} arr 数组
*/
export declare const formatArrToFormData: (formData: FormData, key: string, arr: Array<any>) => void;
/**
 * objToFormData
 * @license https://github.com/therealparmesh/object-to-formdata
 * @param object
 * @param options
 * @param existingFormData
 * @param keyPrefix
 * @returns
 */
export declare const objToFormData: <T = {}>(object: T, options?: Options, existingFormData?: FormData, keyPrefix?: string) => FormData;
