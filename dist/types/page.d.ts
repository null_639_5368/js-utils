/**
* 获取页面高度
* @returns {number} 页面高度
*/
export declare function getPageHeight(): number;
/**
* 获取页面宽度
* @returns {number} 页面宽度
*/
export declare function getPageWidth(): number;
/**
* 元素是否在视窗内
* @param {*} el
* @returns {boolean} true 元素在视窗内 false 元素不在视窗内
*/
export declare function isInViewPort(el: HTMLElement): boolean;
