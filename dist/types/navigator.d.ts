/**
* 判断是否移动设备访问
* @returns {boolean} true 移动设备  false pc设备
*/
export declare function isMobile(): boolean;
/**
* 判断是否微信内置浏览器环境
* @returns {boolean} true 微信环境  false 浏览器
*/
export declare function isWeixin(): boolean;
/**
* 判断运行环境是安卓还是IOS
* @returns  {boolean} true => 安卓 false => IOS
*/
export declare function isAndroid(): boolean;
