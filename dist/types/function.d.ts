/**
* @desc 函数防抖
* @param func 目标函数
* @param wait 延迟执行毫秒数
* @param immediate true - 立即执行， false - 延迟执行
*/
export declare function debounce(func: Function, wait: number, immediate: boolean): Function;
/**
* 深拷贝
* @param {*} obj
*/
export declare function deepClone(obj: any): [] | Object;
/**
* 延迟加载方法
* @param {Function} fn
* @param {number} time
*/
export declare function submitTimeOut(fn: Function, time: number): void;
/**
* 函数节流
* @param {*} fn
* @param {*} interval
* @param {*} isImmediate
*/
export declare function throttle(fn: Function, wait?: number, isImmediate?: boolean): Function;
